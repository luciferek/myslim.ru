<?php 
	namespace App\Controllers;
	use App\Controllers\Controller;
	use App\Models\HomeModel;

	/*
	 * Class which show home page 
	 */
	class HomeController extends Controller 
	{
		/**
		 * Select first row from table home_news 
		 * and transfer it to twig view
		 * @param $request
		 * @param $response
		 * @return  View compilating homepage.twig 
		 **/
		public function sayHello($request, $response) 
		{
			$user = HomeModel::find(1);
			return $this->view->render( $response, 'homepage.twig');
		}
	}

 ?>